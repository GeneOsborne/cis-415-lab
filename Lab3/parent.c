#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

#define UNUSED __attribute__((unused))
#define CHILDREN 8


void handleAlarm(int sig){
	signal(sig, handleAlarm); 
}


int main(UNUSED int argc, char ** argv){
	pid_t pid[CHILDREN];
	int i;

	for( i = 0; i < CHILDREN; i++){
		pid[i] = fork();
		if(pid[i] < 0){
			fprintf(stderr, "Fork() failed to create child %d.\n", i);
			exit(1);
		}
		else if(pid[i] > 0){
			//parent
		}	
		else{
			pause();
			execvp("./child.o", argv);
			fprintf(stderr, "Child %d failed to execute!\n", i);
			exit(127);
		}
	}
	//Parent should be only process executing past this point...

	for( i = 0; i < CHILDREN; i++){
		waitpid(pid[i], 0, 0);
	}
	return 0;
}
