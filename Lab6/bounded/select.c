#include "bqueue.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define BUFFER_COUNT 4
#define NUM_THREADS 8

BQueue ** buffers;
int thread_ids[NUM_THREADS];

int produce(int * n){
	//adds int n to first buffer that is not full
	//If all are full, add to the first one that is no longer full

	//example of how to use bq_add
	bq_add(buffers[0],n); 
}

int * consume(){
	//removes an int from the first buffer that has data available

	//example of how to use bq_remove
	void ** n = NULL;
	bq_remove(buffers[0],n);
	return (int*)(n);

}

void * pro_thread(void * arg){
	long id = (long) arg;
	int i = 100;
	int * t = NULL;
	while( i > 0){
		t = (int*)(malloc(sizeof(int)));
		*t = 100;
		produce(t);
		printf("Thread %ld produced %d so far.!\n", id, i);
		i--;
	}
	pthread_exit(NULL);
}

void * con_thread(void * arg){
	long id = (long) arg;
	int * a;

	//Change this loop so it stops running when you know that nothing else can be added to a queue.
	for(;;){	
		a = consume();
		if(a == NULL) continue;
		printf("Thread %ld consumed: %d!\n", id, *a);
		if(a > 0) {
			*a--;
			produce(a);
			printf("Thread %d produced %d!\n", id, *a);
		}	
	}
}

int main() {
	buffers = (BQueue**)malloc(sizeof(BQueue*)*BUFFER_COUNT);
	long i;
	pthread_t threads[NUM_THREADS];
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	for(i = 0; i < BUFFER_COUNT; i++){
		buffers[i] = bq_create(DEFAULT_CAPACITY);
	}
	for(i = 0; i < NUM_THREADS ; i++){
		thread_ids[i] = i;
		if(i & 1)
			pthread_create(threads + i, &attr, pro_thread, (void*)thread_ids[i]);
		else
			pthread_create(threads + i, &attr, con_thread, (void*)thread_ids[i]); 		
	}
	
   	/* Wait for all threads to complete */
   	for (i=0; i<NUM_THREADS; i++) {
   		pthread_join(threads[i], NULL);
   	}
   	printf ("Main(): Waited on %d  threads. Done.\n", NUM_THREADS);


	//CLEANUP!
	for(i=0; i < BUFFER_COUNT;i++){
		bq_destroy(buffers[i], free);
	}

	pthread_attr_destroy(&attr);
	return 0;
}
