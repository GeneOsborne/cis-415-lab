#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void)
{
   int i;
   pid_t fork_return;
   fork_return = fork();
   if (fork_return == 0)
   {
      for (i=0; i<5; ++i)
      {
         sleep(1);
	 printf("Process %d iteration %d.\n", getpid(),i);
      }
   }
   else if(fork_return > 0)
   {
      for (i=0; i<5; ++i)
      {
	 printf("Process %d iteration %d.\n", getpid(),i);
         sleep(1);
      }
      wait(NULL);
   }
   else{
	fprintf(stderr, "%d failed to fork with %d.\n", getpid(), fork_return);
   }
	return 0;
} 
