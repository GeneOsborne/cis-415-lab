#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
//git clone https://GeneOsborne@bitbucket.org/GeneOsborne/cis-415-lab.git

#define CMD_SIZE 64
#define ARGS_SIZE 512

int main() {
	char * fc;
	for(;;){
		fc = (char*) malloc(sizeof(char)* (CMD_SIZE+ARGS_SIZE));
		printf("Lab2i$");
		scanf("%s", fc);
		pid_t pid = fork();    
		if (pid == 0) { 	
			execlp(fc, "", NULL); 
		} else if (pid > 0) { 
			wait(NULL);
		} else {
			fprintf(stderr, "Failed to fork %d.\n", getpid());
		}
	}
}

