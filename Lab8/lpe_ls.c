/* simple-ls: list contents of a directroy, one per line - version 1 */
#include <sys/types.h> /* needed by dirent.h below */
#include <dirent.h> /* for DIR, struct dirent, opendir(), readdir(), closedir() */
#include <errno.h>  /* for errno, sys_errlist[] */
#include <stdio.h>  /* for stderr, fprintf(), printf() */
#include <string.h> /* for strlen() */
int main(int argc, char *argv[]) {
         int i;
	DIR *dd;
	struct dirent *dent;
	if (argc == 1) {
	    argc = 2;
	    argv[1] = ".";
	}
	/* list current working directory */
        for (i = 1; i < argc; i++) {
             int n;
             char *sepstr;
             if ((dd = opendir(argv[i])) == NULL) {
                 fprintf(stderr, "Error opening directory %s (%s)\n",
                         argv[i], sys_errlist[errno]);
                 continue;
             }
             n = strlen(argv[i]) - 1;
             sepstr = (argv[i][n] == '/') ? "" : "/";
             while ((dent = readdir(dd)) != NULL)
                 printf("%s%s%s\n", argv[i], sepstr, dent->d_name);
             closedir(dd);
	}
	return 0;
}
